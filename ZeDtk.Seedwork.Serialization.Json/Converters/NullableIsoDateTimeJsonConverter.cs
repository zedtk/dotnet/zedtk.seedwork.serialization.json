using System;

namespace ZeDtk.Seedwork.Serialization.Json.Converters
{
    /// <summary>
    /// Converter for nullable <see cref="DateTime"/>.
    /// <see cref="AbstractDateTimeJsonConverter.Format"/> is "yyyy-MM-ddTHH:mm:ss".
    /// </summary>
    public class NullableIsoDateTimeJsonConverter : AbstractNullableDateTimeJsonConverter
    {
        protected override string Format => "yyyy-MM-ddTHH:mm:ss";
    }
}
