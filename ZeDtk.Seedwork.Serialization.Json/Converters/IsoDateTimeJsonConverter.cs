using System;

namespace ZeDtk.Seedwork.Serialization.Json.Converters
{
    /// <summary>
    /// Converter for <see cref="DateTime"/>.
    /// <see cref="AbstractDateTimeJsonConverter.Format"/> is "yyyy-MM-ddTHH:mm:ss".
    /// </summary>
    public class IsoDateTimeJsonConverter : AbstractDateTimeJsonConverter
    {
        protected override string Format => "yyyy-MM-ddTHH:mm:ss";
    }
}
